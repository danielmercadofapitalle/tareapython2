import socket
import os
from _thread import *

ServidorSocket = socket.socket()
host = '127.0.0.1'
port = 8080
ThreadCount = 0
try:
    ServidorSocket.bind((host, port))
except socket.error as e:
    print(str(e))

print('Esperando conexion')
ServidorSocket.listen(5)


def threaded_client(connection):
    connection.send(str.encode('Bienvenido'))
    while True:
        data = connection.recv(2048)
        reply = 'Servidor: ' + data.decode('utf-8')
        if not data:
            break
        connection.sendall(str.encode(reply))
    connection.close()

while True:
    Client, address = ServidorSocket.accept()
    print('Conectado: ' + address[0] + ':' + str(address[1]))
    start_new_thread(threaded_client, (Client, ))
    ThreadCount += 1
    print('Cantidad de hilos: ' + str(ThreadCount))	
ServidorSocket.close()

